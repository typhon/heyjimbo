'use strict';

var __awaiter = undefined && undefined.__awaiter || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
            try {
                step(generator.next(value));
            } catch (e) {
                reject(e);
            }
        }
        function rejected(value) {
            try {
                step(generator.throw(value));
            } catch (e) {
                reject(e);
            }
        }
        function step(result) {
            result.done ? resolve(result.value) : new P(function (resolve) {
                resolve(result.value);
            }).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
///<reference path='../typings/main/ambient/node/node.d.ts' />
var fs = require('fs');
var mark_it = require('markdown-it')({
    html: true
});
var promise = require('bluebird').promisify;
var glob = require('glob');
var _ = require('underscore');
_.flatMap = function (arr, func) {
    return _.reduce(arr, function (s, x) {
        return s.concat(func(x));
    }, []);
};
// builds a list of all posts
var all_posts_memo = null;
function get_all_posts() {
    return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee() {
        var data, files, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, filename, postname, content;

        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        if (!(all_posts_memo && all_posts_memo.expires > +new Date())) {
                            _context.next = 2;
                            break;
                        }

                        return _context.abrupt('return', all_posts_memo.value);

                    case 2:
                        data = [];
                        _context.next = 5;
                        return promise(glob)(__dirname + "/../../content/*.meta.json");

                    case 5:
                        files = _context.sent;
                        _iteratorNormalCompletion = true;
                        _didIteratorError = false;
                        _iteratorError = undefined;
                        _context.prev = 9;
                        _iterator = files[Symbol.iterator]();

                    case 11:
                        if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                            _context.next = 28;
                            break;
                        }

                        filename = _step.value;
                        postname = /(\w+)\.meta/.exec(filename);
                        _context.next = 16;
                        return promise(fs.readFile)(filename);

                    case 16:
                        content = _context.sent;
                        content = JSON.parse(content);

                        content.filename = postname[1];
                        _context.next = 21;
                        return get_post(postname[1]);

                    case 21:
                        content.content = _context.sent;

                        content.info = "";
                        content.title = content.title || "Untitled";
                        data.push(content);

                    case 25:
                        _iteratorNormalCompletion = true;
                        _context.next = 11;
                        break;

                    case 28:
                        _context.next = 34;
                        break;

                    case 30:
                        _context.prev = 30;
                        _context.t0 = _context['catch'](9);
                        _didIteratorError = true;
                        _iteratorError = _context.t0;

                    case 34:
                        _context.prev = 34;
                        _context.prev = 35;

                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }

                    case 37:
                        _context.prev = 37;

                        if (!_didIteratorError) {
                            _context.next = 40;
                            break;
                        }

                        throw _iteratorError;

                    case 40:
                        return _context.finish(37);

                    case 41:
                        return _context.finish(34);

                    case 42:
                        data.sort(function (a, b) {
                            return b.created - a.created;
                        });
                        all_posts_memo = { expires: 600000 + +new Date(), value: data };
                        return _context.abrupt('return', data);

                    case 45:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[9, 30, 34, 42], [35,, 37, 41]]);
    }));
}
function get_post(id) {
    return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee2() {
        var path, content;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        path = __dirname + '/../../content/' + id + '.md';
                        _context2.next = 3;
                        return promise(fs.readFile)(path, { 'encoding': 'utf-8' });

                    case 3:
                        content = _context2.sent;
                        return _context2.abrupt('return', mark_it.render(content));

                    case 5:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this);
    }));
}
// given a post name (e.g. "fizzbuzz"), a post object including content, title, and metadata
function post(id) {
    return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee3() {
        var _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, post;

        return regeneratorRuntime.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        _iteratorNormalCompletion2 = true;
                        _didIteratorError2 = false;
                        _iteratorError2 = undefined;
                        _context3.prev = 3;
                        _context3.next = 6;
                        return get_all_posts();

                    case 6:
                        _context3.t0 = Symbol.iterator;
                        _iterator2 = _context3.sent[_context3.t0]();

                    case 8:
                        if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                            _context3.next = 15;
                            break;
                        }

                        post = _step2.value;

                        if (!(post.filename === id)) {
                            _context3.next = 12;
                            break;
                        }

                        return _context3.abrupt('return', post);

                    case 12:
                        _iteratorNormalCompletion2 = true;
                        _context3.next = 8;
                        break;

                    case 15:
                        _context3.next = 21;
                        break;

                    case 17:
                        _context3.prev = 17;
                        _context3.t1 = _context3['catch'](3);
                        _didIteratorError2 = true;
                        _iteratorError2 = _context3.t1;

                    case 21:
                        _context3.prev = 21;
                        _context3.prev = 22;

                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }

                    case 24:
                        _context3.prev = 24;

                        if (!_didIteratorError2) {
                            _context3.next = 27;
                            break;
                        }

                        throw _iteratorError2;

                    case 27:
                        return _context3.finish(24);

                    case 28:
                        return _context3.finish(21);

                    case 29:
                        throw "Not Found: " + id;

                    case 30:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[3, 17, 21, 29], [22,, 24, 28]]);
    }));
}
function posts(tag, num) {
    return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
        var all_posts, posts;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        _context4.next = 2;
                        return get_all_posts();

                    case 2:
                        all_posts = _context4.sent;
                        posts = all_posts.filter(function (post) {
                            return post.tags && (!tag || ~post.tags.indexOf(tag));
                        });

                        if (num) posts = posts.slice(0, num);
                        _context4.t0 = posts;
                        _context4.t1 = _.chain(all_posts).sortBy(function (x) {
                            return -x.created;
                        }).value();
                        _context4.next = 9;
                        return tags(all_posts);

                    case 9:
                        _context4.t2 = _context4.sent.tags;
                        return _context4.abrupt('return', {
                            posts: _context4.t0,
                            recent: _context4.t1,
                            tags: _context4.t2
                        });

                    case 11:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this);
    }));
}
function tags(posts) {
    return __awaiter(this, void 0, void 0, regeneratorRuntime.mark(function _callee5() {
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        _context5.t0 = posts;

                        if (_context5.t0) {
                            _context5.next = 5;
                            break;
                        }

                        _context5.next = 4;
                        return get_all_posts();

                    case 4:
                        _context5.t0 = _context5.sent;

                    case 5:
                        posts = _context5.t0;
                        return _context5.abrupt('return', { tags: _.uniq(_.flatMap(posts, function (p) {
                                return p.tags;
                            })) });

                    case 7:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, this);
    }));
}
exports.post = post;
exports.posts = posts;
exports.tags = tags;