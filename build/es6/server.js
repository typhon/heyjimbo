///<reference path='../typings/main/ambient/node/node.d.ts' />
///<reference path='../typings/main/ambient/mime/mime.d.ts' />
///<reference path='../typings/main/ambient/serve-static/serve-static.d.ts' />
///<reference path='../typings/main/ambient/express/express.d.ts' />
require('babel-polyfill');
var content_db = require('./content_db');
var Feed = require('feed');
var promise = require('bluebird').promisify;
var express = require('express');
var app = express(), port = 8081;
app.set('view engine', 'ejs');
app.set('views', __dirname + '/../../views');
app.use('/styles', express.static(__dirname + '/../../styles'));
app.use('/images', express.static(__dirname + '/../../images'));
app.use(express.static(__dirname + '/../../httproot'));
function maproute(req, resp, controller_promise, template) {
    controller_promise
        .then(data => promise(resp.render, { context: resp })(template, data))
        .then(html => {
        resp.header("Content-Type", "text/html");
        resp.end(html);
    })
        .catch(e => {
        console.error(e.stack);
        resp.writeHead(500);
        resp.end("" + e);
    });
}
app.get('/', (req, resp) => maproute(req, resp, content_db.posts(null, 5), "home"));
app.get('/tagged/:tag', (req, resp) => maproute(req, resp, content_db.posts(req.params.tag), "home"));
app.get('/tags', (req, resp) => maproute(req, resp, content_db.tags(), "tags"));
app.get('/post/:id', (req, resp) => maproute(req, resp, content_db.post(req.params.id), "post"));
app.get('/atom.xml', (req, resp) => {
    var feed = new Feed({
        title: 'HeyJimbo',
        description: 'Adventures in Programming with Jimmy Tang',
        link: 'http://heyjimbo.com/',
        id: 'http://heyjimbo.com/',
        author: { name: 'Jimmy Tang' },
    });
    (content_db.posts(null, 10)
        .then(data => {
        data.posts.map(item => {
            feed.addItem({
                title: item.title,
                link: 'http://heyjimbo.com/post/' + item.filename,
                id: 'http://heyjimbo.com/post/' + item.filename,
                description: item.content,
                date: new Date(item.created),
            });
        });
        resp.end(feed.render('atom-1.0'));
    })
        .catch(e => {
        resp.writeHead(500);
        resp.end('oops' + e);
        console.error(e.stack);
    }));
});
console.log("Starting app on port", port);
app.listen(port);
