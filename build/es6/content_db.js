var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};
///<reference path='../typings/main/ambient/node/node.d.ts' />
var fs = require('fs');
var mark_it = require('markdown-it')({
    html: true,
});
var promise = require('bluebird').promisify;
var glob = require('glob');
var _ = require('underscore');
_.flatMap = (arr, func) => _.reduce(arr, (s, x) => s.concat(func(x)), []);
// builds a list of all posts
var all_posts_memo = null;
function get_all_posts() {
    return __awaiter(this, void 0, void 0, function* () {
        if (all_posts_memo && all_posts_memo.expires > +new Date())
            return all_posts_memo.value;
        var data = [];
        var files = yield promise(glob)(__dirname + "/../../content/*.meta.json");
        for (var filename of files) {
            var postname = /(\w+)\.meta/.exec(filename);
            var content = yield promise(fs.readFile)(filename);
            var content = JSON.parse(content);
            content.filename = postname[1];
            content.content = yield get_post(postname[1]);
            content.info = "";
            content.title = content.title || "Untitled";
            data.push(content);
        }
        data.sort(function (a, b) { return b.created - a.created; });
        all_posts_memo = { expires: 600000 + +new Date(), value: data };
        return data;
    });
}
function get_post(id) {
    return __awaiter(this, void 0, void 0, function* () {
        var path = __dirname + '/../../content/' + id + '.md';
        var content = yield promise(fs.readFile)(path, { 'encoding': 'utf-8' });
        return mark_it.render(content);
    });
}
// given a post name (e.g. "fizzbuzz"), a post object including content, title, and metadata
function post(id) {
    return __awaiter(this, void 0, void 0, function* () {
        for (var post of (yield get_all_posts()))
            if (post.filename === id)
                return post;
        throw "Not Found: " + id;
    });
}
function posts(tag, num) {
    return __awaiter(this, void 0, void 0, function* () {
        var all_posts = yield get_all_posts();
        var posts = all_posts.filter(post => post.tags && (!tag || ~post.tags.indexOf(tag)));
        if (num)
            posts = posts.slice(0, num);
        return {
            posts: posts,
            recent: _.chain(all_posts).sortBy(x => -x.created).value(),
            tags: (yield tags(all_posts)).tags,
        };
    });
}
function tags(posts) {
    return __awaiter(this, void 0, void 0, function* () {
        posts = posts || (yield get_all_posts());
        return { tags: _.uniq(_.flatMap(posts, p => p.tags)) };
    });
}
exports.post = post;
exports.posts = posts;
exports.tags = tags;
