///<reference path='../typings/main/ambient/node/node.d.ts' />
var fs = require('fs');
var mark_it = require('markdown-it')({
    html: true,
});
var promise = require('bluebird').promisify
var glob = require('glob');
var _ = require('underscore');
_.flatMap = (arr, func) => _.reduce(arr, (s,x) => s.concat(func(x)), []);

// builds a list of all posts
var all_posts_memo = null;
async function get_all_posts() {
    if (all_posts_memo && all_posts_memo.expires > +new Date()) return all_posts_memo.value;

    var data = [];
    var files = await promise(glob)(__dirname + "/../../content/*.meta.json");
    for(var filename of files) {
        var postname = /(\w+)\.meta/.exec(filename);
        var content = await promise(fs.readFile)(filename);
        var content = JSON.parse(content);
        content.filename = postname[1];
        content.content = await get_post(postname[1]);
        content.info = "";
        content.title = content.title || "Untitled";
        data.push(content);
    }
    data.sort(function(a,b) { return b.created - a.created; });
    all_posts_memo = {expires: 600000 + +new Date(), value: data};
    return data;
}

async function get_post(id) {
    var path = __dirname + '/../../content/' + id + '.md';
    var content = await promise(fs.readFile)(path, {'encoding': 'utf-8'});
    return mark_it.render(content);
}
// given a post name (e.g. "fizzbuzz"), a post object including content, title, and metadata
async function post(id) {
    for (var post of (await get_all_posts()))
        if (post.filename === id)
            return post;
    throw "Not Found: " + id;
}

async function posts(tag, num) {
    var all_posts = await get_all_posts();
    var posts = all_posts.filter(post => post.tags && (!tag || ~post.tags.indexOf(tag)));
    if (num) posts = posts.slice(0, num);
    return {
        posts: posts,
        recent: _.chain(all_posts).sortBy(x => -x.created).value(),
        tags: (await tags(all_posts)).tags,
    };
}

async function tags(posts) {
    posts = posts || await get_all_posts();
    return { tags: _.uniq(_.flatMap(posts, p => p.tags)) };
}

exports.post = post;
exports.posts = posts;
exports.tags = tags;
